package gr.uoc.radio.fosscomm.utils;

import android.content.Context;

import java.text.DateFormat;
import java.util.TimeZone;

public class DateUtils {

	private static final TimeZone GREECE_TIME_ZONE = TimeZone.getTimeZone("GMT+3");

	public static TimeZone getGreeceTimeZone() {
		return GREECE_TIME_ZONE;
	}

	public static DateFormat withGreeceTimeZone(DateFormat format) {
		format.setTimeZone(GREECE_TIME_ZONE);
		return format;
	}

	public static DateFormat getTimeDateFormat(Context context) {
		return withGreeceTimeZone(android.text.format.DateFormat.getTimeFormat(context));
	}
}
