package gr.uoc.radio.fosscomm.api;

import java.util.Locale;

/**
 * This class contains all FOSSCOMM Urls
 *
 * @author Christophe Beyls
 */
public class FosscommUrls {

	private static final String SCHEDULE_URL = "https://fosscomm2018.gr/app/schedule.xml";
	private static final String ROOMS_URL = "";
	private static final String EVENT_URL_FORMAT = "https://fosscomm%1$d.gr/fs2018/talk/%2$s/";
	private static final String PERSON_URL_FORMAT = "https://fosscomm%1$d.gr/fs2018/speaker/%2$s/";
	private static final String VOLUNTEER_URL = "https://fosscomm2018.gr/index.php/2018/03/10/628/";

	public static String getSchedule() {
		return SCHEDULE_URL;
	}

	public static String getRooms() {
		return ROOMS_URL;
	}

	public static String getEvent(String slug, int year) {
		return String.format(Locale.US, EVENT_URL_FORMAT, year, slug);
	}

	public static String getPerson(String slug, int year) {
		return String.format(Locale.US, PERSON_URL_FORMAT, year, slug);
	}

	public static String getVolunteer() {
		return VOLUNTEER_URL;
	}
}
