package gr.uoc.radio.fosscomm.model;

import android.text.TextUtils;

import java.util.Arrays;

public class Building {

	public static final String[] CSDRooms = {"Α1", "Α2", "Α3", "Α4", "Αμφιθέατρο Α", "Αμφιθέατρο Β", "Αναγνωστήριο"};
	public static final String[] MathRooms = {"Αμφιθέατρο Ν. Πετρίδης"};
	public static final String[] OtherRooms = {"Φοιτητικό Κέντρο"};

	public static String fromRoomName(String roomName) {
		if (!TextUtils.isEmpty(roomName)) {
			if (Arrays.asList(CSDRooms).contains(roomName))
				return "Επιστήμης Υπολογιστών";
			if (Arrays.asList(MathRooms).contains(roomName))
				return "Μαθηματικών & Εφαρμοσμένων Μαθηματικών";
			if (Arrays.asList(OtherRooms).contains(roomName))
				return "Φοιτητικό Κέντρο";
		}
		return "Άγνωστο";
	}
}
